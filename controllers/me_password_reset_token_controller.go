package controllers

import (
	"net/http"

	"github.com/labstack/echo"
)

func ShowPasswordResetToken(c echo.Context) (err error) {
	return c.String(http.StatusOK, "NOT IMPLEMENTED")
}

func CreatePasswordResetToken(c echo.Context) (err error) {
	return c.String(http.StatusOK, "NOT IMPLEMENTED")
}

func DestroyPasswordResetToken(c echo.Context) (err error) {
	return c.String(http.StatusOK, "NOT IMPLEMENTED")
}
