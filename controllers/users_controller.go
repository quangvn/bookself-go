package controllers

import (
	// "fmt"
	"net/http"
	"strconv"
	"github.com/labstack/echo"
	"github.com/peara/bookself/models"
	"github.com/peara/bookself/database"
)

// UsersController /users
type UsersController struct {
	BaseController
}

// Index return list of users
func (*UsersController) Index(c echo.Context) (err error) {
	email := c.QueryParam("email")
	users := models.FetchByEmail(email)
	return c.JSON(http.StatusOK, users)
}

// Create creates a user using submitted parameters
func (*UsersController) Create(c echo.Context) (err error) {
	// fmt.Println("UsersController.Create")
	u := new(models.User)
	if err := c.Bind(u); err != nil {
		return err
	}

	if err := models.Create(u); err != nil {
		return err
	}
	return c.JSON(http.StatusOK, u)
}

// Show returns detail information of a user
func (*UsersController) Show(c echo.Context) (err error) {
	userID, _ := strconv.Atoi(c.Param("user_id"))
	u, err := models.FetchByID(userID)
	if err != nil {
		return err
	}
	return c.JSON(http.StatusOK, u)
}

// Update updates a user information
func (*UsersController) Update(c echo.Context) (err error) {
	id, _ := strconv.Atoi(c.Param("user_id"))
	u := new(models.User)
	database.DB.First(u, id)

	if err := c.Bind(u); err != nil {
		return err
	}

	if err := models.Save(u); err != nil {
		return err
	}
	return c.JSON(http.StatusOK, u)
}

// Destroy deletes a user from database
func (*UsersController) Destroy(c echo.Context) (err error) {
	// TODO error handling
	userID, _ := strconv.Atoi(c.Param("user_id"))
	models.Delete(userID)
	return c.JSON(http.StatusOK, nil)
}
