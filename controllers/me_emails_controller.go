package controllers

import (
	"net/http"
	"github.com/labstack/echo"
)

func IndexEmails(c echo.Context) (err error) {
	return c.String(http.StatusOK, "NOT IMPLEMENTED")
}

func CreateEmail(c echo.Context) (err error) {
	return c.String(http.StatusOK, "NOT IMPLEMENTED")
}

func UpdateEmail(c echo.Context) (err error) {
	return c.String(http.StatusOK, "NOT IMPLEMENTED")
}

func DestroyEmail(c echo.Context) (err error) {
	return c.String(http.StatusOK, "NOT IMPLEMENTED")
}
