# Base Golang api server

- Sample user model, controller
- Migration files
- Database connection
- Fresh
- Test

## Technology stack

- [Go v1.8.3](https://golang.org/)
- [MySQL](https://www.mysql.com/)
- [Echo](https://echo.labstack.com/): Micro framework
- [GNU make](https://www.gnu.org/software/make/): Task runner
- [migrate/cli v3.0.1](https://github.com/mattes/migrate/tree/v3.0.1/cli): Database migration command
- [glide](https://github.com/Masterminds/glide): Vendor Package Management

Other libraries are listed in glide.lock.

## Development

### Setup

- Install [Go](https://golang.org/) and set `GOPATH`.
- Install [migrate v3.0.1](https://github.com/mattes/migrate/)
- Install [fresh](https://github.com/pilu/fresh)
- Create database `v2_auth`. We assume that there is a `root` user and that there is no password for `root`.
- Execute the following command.

```
git clone git@github.com:peara/bookself.git {$GOPATH}/src/github.com/peara/bookself
cd {$GOPATH}/src/github.com/peara/bookself
make setup
make migrate
fresh
```

### Run test

```
make test
```

### Commit Message Format

Each commit message consists of some types, a subject.

```
<type>: <subject>
<BLANK LINE>
<body>
```

#### Type

Type Must be one of the following:

* **feat**: A new feature
* **fix**: A bug fix
* **workaround**: A workaround
* **doc**: Documentation only changes
* **style**: Changes that do not affect the meaning of the code (white-space, formatting, missing semi-colons, etc)
* **refactor**: A code change that neither fixes a bug nor adds a feature
* **perf**: A code change that improves performance
* **test**: Adding missing or correcting existing tests
* **chore**: Changes to the build process or auxiliary tools and libraries such as documentation generation


### Coding rules

- Use [migrate](https://github.com/mattes/migrate/tree/v3.0.1/cli) for database migration. Don't use [gorm's migration](http://jinzhu.me/gorm/database.html#migration).

## URLs

- [echo guide](https://echo.labstack.com/guide)
- [echo cookbook](https://echo.labstack.com/cookbook)
- [gorm document](http://jinzhu.me/gorm/)
