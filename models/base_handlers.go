package models

import (
	// "github.com/k0kubun/pp"
	"github.com/peara/bookself/database"
)

// FetchByEmail fetchs user from database using email
func FetchByEmail(email string) []User {
	var users []User
	database.DB.Where("email = ?", email).Find(&users)
	return users
}

// FetchByID fetchs user from database using id
func FetchByID(id int) (*User, error) {
	u := new(User)
	return u, database.DB.First(u, id).Error
}

func Create(record interface{}) error {
	return database.DB.Create(record).Error
}

func Save(record interface{}) error {
	return database.DB.Save(record).Error
}

// Delete takes an id and delete the corresponding user from database, return the user
func Delete(id int) User {
	var user User
	database.DB.Where("id = ?", id).First(&user)
	database.DB.Delete(&user)
	return user
}
