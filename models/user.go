package models

import (
	// "fmt"
	"time"
)

// User is orm of user table
type User struct {
	ID                 uint       `gorm:"primary_key" json:"id"`
	CreatedAt          time.Time  `json:"created_at"`
	UpdatedAt          time.Time  `json:"updated_at"`
	DeletedAt          *time.Time `json:"deleted_at"`
	UID                string     `json:"uid"`
	Email              string     `json:"email"`
	Password           string     `json:"password"`
	ResetPasswordToken string     `json:"reset_password_token"`
	ResetPasswordAt    *time.Time  `json:"reset_password_at"`
}
