package route

import (
	"net/http"

	"github.com/labstack/echo"
	"github.com/labstack/echo/middleware"
	c "github.com/peara/bookself/controllers"
)

func Init() *echo.Echo {
	e := echo.New()
	e.Use(middleware.CORS())

	e.GET("/ping", func(c echo.Context) error {
		return c.String(http.StatusOK, "pong")
	})

	var (
		usersController c.UsersController
	)

	v1 := e.Group("/v1")
	{
		// v1.POST("/me/token", controllers.CreateToken)
		// v1.PUT("/me/token", controllers.UpdateToken)

		// v1.PUT("/me/password", controllers.UpdatePassword)

		// v1.GET("/me/password_reset_token", controllers.ShowPasswordResetToken)
		// v1.POST("/me/password_reset_token", controllers.CreatePasswordResetToken)
		// v1.DELETE("/me/password_reset_token", controllers.DestroyPasswordResetToken)

		// v1.POST("/me/emails", controllers.CreateEmail)
		// v1.GET("/me/emails", controllers.IndexEmails)
		// v1.DELETE("/me/emails", controllers.DestroyEmail)
		// v1.PUT("/me/emails", controllers.UpdateEmail)

		v1.GET("/users", usersController.Index)
		v1.POST("/users", usersController.Create)
		v1.GET("/users/:user_id", usersController.Show)
		v1.PUT("/users/:user_id", usersController.Update)
		v1.DELETE("/users/:user_id", usersController.Destroy)
	}
	return e
}
