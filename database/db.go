package database

import (
	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/mysql"
)

var DB *gorm.DB

func Connect() *gorm.DB {
	db, err := gorm.Open("mysql", "root:12341234@tcp(172.17.0.2:3306)/bookself?charset=utf8&parseTime=True&loc=Local")
	db.LogMode(true)
	if err != nil {
		panic(err.Error() + " failed to connect database")
	}
	return db
}

func Close(d *gorm.DB) error {
	return d.Close()
}

func Init() {
	DB = Connect()
}

func Finalize() {
	DB.Close()
}
