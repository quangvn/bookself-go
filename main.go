package main

import (
	"github.com/peara/bookself/database"
	"github.com/peara/bookself/route"
	"github.com/sirupsen/logrus"
)

func init() {
	logrus.SetLevel(logrus.DebugLevel)
	logrus.SetFormatter(&logrus.JSONFormatter{})
}

func main() {
	router := route.Init()
	database.Init()
	defer database.Finalize()
	router.Logger.Fatal(router.Start(":1323"))
}
